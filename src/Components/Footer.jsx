export default function Footer() {
    return (
        <div className="footer">
            <span> &copy; All rights reserved; <a href="https://sauravjaiswalsj.github.io/">  &copy;sauravjaiswalsj</a></span>
            <br />
            <span> Made with ❤️  </span>
        </div>
    );
}